'use strict';


var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: './dev/main.js',
	output: {
		path: './build/',
		filename: 'build.js'
	},
	module: {
		loaders: [{
			test: /\.html$/,
			loader: 'html-loader'
		}, {	
			test: /\.styl$/,
			loader: ExtractTextPlugin.extract('stylus', "css-loader!autoprefixer-loader!stylus-loader")
		}],
	},
 	plugins: [
        new ExtractTextPlugin("build.css", {
            allChunks: true
        })
    ],
	watch: true
}