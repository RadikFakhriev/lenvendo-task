'use strict';

module.exports = Observer;


function Observer (sender) {
	var self = this;

	self._sender = sender;
    self._listeners = [];
};


Observer.prototype = {
    attach: function (listener) {
        this._listeners.push(listener);
    },

    detachListners: function () {
        this._listeners.splice(0, this._listeners.length);
    },
    
    notify: function (args) {
        var index;

        for (index = 0; index < this._listeners.length; index += 1) {
            this._listeners[index](this._sender, args);
        }
    }
};