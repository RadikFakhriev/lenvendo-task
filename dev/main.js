'use strict';

require('./common/common.styl');

var TextBloksListModel = require('./TextBlocksList/TextBlocksListModel'),
	TextBloksListView = require('./TextBlocksList/TextBlocksListView'),
	TextBlocksListController = require('./TextBlocksList/TextBlocksListController');

var listModel = new TextBloksListModel([]),
	appView = new TextBloksListView(listModel, document.getElementById('TextBlocksBoard')),
	listController = new TextBlocksListController(listModel, appView);

appView.render();

