'use strict';


module.exports = TextBlockItemFacade;

var TextBlockItemView = require('./TextBlockItemView'),
	TextBlockItemModel = require('./TextBlockItemModel'),
	TextBlockItemController = require('./TextBlockItemController');


function TextBlockItemFacade(item) {
	var textBlockModel;

	if (item.isDifficult) {
		textBlockModel =  new TextBlockItemModel.Difficult(item);
	} else {
		textBlockModel = new TextBlockItemModel.Usual();
	} 

	var	textBlockView = new TextBlockItemView(textBlockModel),
		textBlockController = new TextBlockItemController(textBlockModel, textBlockView);

	return {
		model: textBlockModel,
		view: textBlockView,
		controller: textBlockController
	};
};