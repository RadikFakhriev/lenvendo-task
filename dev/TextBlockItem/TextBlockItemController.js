'use strict';


module.exports = TextBlockItemController;


function TextBlockItemController(model, view) {
	var self = this;

	self._model = model;
	self._view = view;

	self._view.delButtonClicked.attach(function() {
		self.removeCurrentItem();
	});

	self._view.blockSelected.attach(function () {
		self.selectCurrentItem();
	});

	self._view.stateChanged.attach(function () {
		self.changeStateOfCurrentItem();
	});
};


TextBlockItemController.prototype = {
	selectCurrentItem: function () {
		this._model.select();
	},

	removeCurrentItem: function () {
		var answer = window.confirm('Вы действительно хотите удалить этот блок?');
		answer ? this._model.remove() : null;
	},

	changeStateOfCurrentItem: function () {
		this._model.changeState();
	}

};