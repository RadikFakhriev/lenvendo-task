'use strict';


module.exports = TextBlockItemView;

require('./textBlockItem.styl');

var blockTemplate = require('./TextBlockItem.tpl.html'),
	Observer = require('../common/Observer'),
	TextBlockItemModel = require('./TextBlockItemModel');


function TextBlockItemView(model) {
	var self = this;

	self._model = model;

	
	self.delButtonClicked = new Observer(self);
	self.blockSelected = new Observer(self);
	self.stateChanged = new Observer(self);

	self.delButtonHook = function () {
		self.delButtonClicked.notify();
	}

	self.selectBlockHook = function () {
		self.blockSelected.notify();
	}

	self.changeStateOfDifficultBlockHook = function () {
		self.stateChanged.notify();
	}
};


TextBlockItemView.prototype = {
	renderTo: function (wrapper) {
		var that = this,
			temp = document.createElement('div'),
			thatBlockIsDifficult = that._model instanceof TextBlockItemModel.Difficult;

		temp.innerHTML = blockTemplate;
		that._elementContainer = temp.firstChild;
		that._element = temp.querySelector('.text-block');
		that._element.querySelector('.text-block__content').innerHTML = that._model.content;



		if (thatBlockIsDifficult) {
			that._element.classList.add('text-block--difficult')
		}

		if (that._model.isSelected) {
			that._element.classList.add('text-block--selected');
		}
		
		if (!that._model.isRed && thatBlockIsDifficult) {
			that._element.classList.add('text-block--green');
		} else if (that._model.isRed) {
			that._element.classList.add('text-block--red');
		}

		wrapper.appendChild(that._elementContainer);
	},

	getElement: function () {
		return this._element;
	},

};


