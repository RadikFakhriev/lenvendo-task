'use strict';


module.exports = {
	Usual: TextBlockItemModel,
	Difficult: DifficultTextBlockItemModel
};

var Observer = require('../common/Observer');

var currentId = 0;


function TextBlockItemModel() {
	var self = this;

	self.id = self.generateId();
	self.onRemoved = new Observer(self);
	self.onSelected = new Observer(self);
	self.content = "";
};


TextBlockItemModel.prototype = {
	generateId: function () {
		var newId = ++currentId;
		return newId;
	},

	getId: function () {
		return this.id;
	},

	getItem: function () {
		return this;
	},

	select: function () {
		this.isSelected = !this.isSelected;
		this.onSelected.notify();
	},

	remove: function () {
		this.onRemoved.notify();
	}
};


function DifficultTextBlockItemModel() {
	var self = this;

	self.id = self.generateId();
	self.onRemoved = new Observer(self);
	self.onSelected = new Observer(self);
	self.onStateChanged = new Observer(self);
	self.isRed = true;

}

DifficultTextBlockItemModel.prototype = Object.create(TextBlockItemModel.prototype);

DifficultTextBlockItemModel.prototype.changeState = function () {
	var that = this;

	that.isRed = !that.isRed;
	that.onStateChanged.notify();
}