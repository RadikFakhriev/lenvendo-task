'use strict';

module.exports = TextBlocksListController;



function TextBlocksListController(model, view) {
    var self = this;

	self._model = model;
    self._view = view;


    self._view.addButtonClicked.attach(function(sender, args) {
        self.addItem(args);
    });

};


TextBlocksListController.prototype = {
    addItem: function (newItemProps) {
        this._model.addItem(newItemProps);
    },
   
};