'use strict';

module.exports = TextBlocksListView;

require('./textBlocksList.styl');
require('../common/ManipulatorDOM');

var Observer = require('../common/Observer');


function TextBlocksListView (model, element) {
	var self = this;

	self._model = model;
    self._elements = element;

    self.listModified = new Observer(self);
    self.addButtonClicked = new Observer(self);
    self.delButtonDelegateClick = new Observer(self);

    
    self._model.itemAdded.attach(function () {
        self.rebuildList();
    });
    self._model.itemRemoved.attach(function () {
        self.rebuildList();
    });
    self._model.itemSelected.attach(function () {
        self.rebuildList();
    });
    self._model.itemStateChanged.attach(function () {
        self.rebuildList();
    });

    self._elements.querySelector('#addButton').addEventListener('click', function () {
        this.parentNode.querySelector('.embedded-actions').classList.toggle('embedded-actions--open');
    });


    self._elements.querySelector('.embedded-actions').addEventListener('click', function (event) {
        var targetNode = event.target,
            buttonNode = targetNode.closest('.embedded-action');

        if (buttonNode.classList.contains('embedded-actions__create-usual')) {
            self.addButtonClicked.notify({isDifficult: false});
        } else if(buttonNode.classList.contains('embedded-actions__create-difficult')) {
            self.addButtonClicked.notify({isDifficult: true});
        } else {
            return;
        }
    });
    
    var timer = 0;
    var delay = 200;
    var prevent = false;
    self._elements.querySelector('#BlocksContainer').addEventListener('click', function(event) {
        timer = setTimeout(function() {
            if (!prevent) {
                listAreaClickAction(event);
            }
            prevent = false;
        }, delay);
    });
    self._elements.querySelector('#BlocksContainer').addEventListener('dblclick', function(event) {
        clearTimeout(timer);
        prevent = true;
        listAreaDoubleClickAction(event);
    });


    function listAreaClickAction(event) {
        var targetNode = event.target,
            delButtonNode = targetNode.closest('.text-block-bar__delete-btn'),
            blockNodeContainer = targetNode.closest('.blocks-list__item'),
            blockNode = targetNode.closest('.text-block'),
            siblingIndex = 0;

        var fabState = self._elements.querySelector('.embedded-actions').classList;

        if (fabState.contains('embedded-actions--open')) {
            fabState.toggle('embedded-actions--open');
        }
        

        if (delButtonNode) {
            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
            self.delButtonDelegateClick.notify({removedBlockIndex: siblingIndex});
        } else if(blockNode && !targetNode.classList.contains('text-block-bar__delete-btn')) {
            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
            self.listModified.notify({selectedBlockIndex: siblingIndex});
        } else {
            return;
        }
       
    };

    function listAreaDoubleClickAction(event) {
        var targetNode = event.target,
            delButtonNode = targetNode.closest('.text-block-bar__delete-btn'),
            blockNodeContainer = targetNode.closest('.blocks-list__item'),
            blockNode = targetNode.closest('.text-block'),
            siblingIndex = 0;

        var fabState = self._elements.querySelector('.embedded-actions').classList;

        if (fabState.contains('embedded-actions--open')) {
            fabState.toggle('embedded-actions--open');
        }
        

        if (delButtonNode) {
            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
            self.delButtonDelegateClick.notify({removedBlockIndex: siblingIndex});
        } else if(blockNode && !targetNode.classList.contains('text-block-bar__delete-btn') && blockNode.classList.contains('text-block--difficult')) {
            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
            self.listModified.notify({changedStateBlockIndex: siblingIndex});
        } else {
            return;
        }
    };
};


TextBlocksListView.prototype = {
	render: function () {
        this.rebuildList();
    },

    rebuildList: function () {
        var list,
        	that = this,
        	items,
        	key,
        	textBlockComponent,
            blockState,
        	statePanel,
            amountSelectedBlocks = 0,
            amountRedBlocks = 0,
            amountGreenBlocks = 0,
            amountSelectedString = ", выбрано: ",
            amountRedBlocksString = ", красных: ",
            amountGreenBlocksString = ", зелёных: ";

        list = document.getElementById('BlocksContainer');
        list.innerHTML = '';
        statePanel = that._elements.querySelector('.top-bar__state-panel');


        that.delButtonDelegateClick.detachListners();
        that.listModified.detachListners();

        items = that._model.getItems();

        for (key in items) {
			items[key].view.renderTo(list);
            if (items[key].model.isSelected) {
                amountSelectedBlocks++;
            }
            if (items[key].model.isRed) {
                amountRedBlocks++;
            }
            if (items[key].model.isRed === false) {
                amountGreenBlocks++;
            }
        }

        that.delButtonDelegateClick.attach(function (sender, args) {
        	items[args.removedBlockIndex].view.delButtonHook();
        });

        that.listModified.attach(function (sender, args) {
        	if (args.hasOwnProperty('selectedBlockIndex')) {
        		items[args.selectedBlockIndex].view.selectBlockHook();
        	}

            if (args.hasOwnProperty('changedStateBlockIndex')) {
                items[args.changedStateBlockIndex].view.changeStateOfDifficultBlockHook();
            }
        });

        amountSelectedString = amountSelectedBlocks ? amountSelectedString + amountSelectedBlocks : '';
        amountRedBlocksString = amountRedBlocks ? amountRedBlocksString + amountRedBlocks : '';
        amountGreenBlocksString = amountGreenBlocks ? amountGreenBlocksString + amountGreenBlocks : '';
     	statePanel.innerHTML = "  Общее количество блоков: " + items.length + amountSelectedString + amountRedBlocksString + amountGreenBlocksString;

    }

};
