'use strict';

module.exports = TextBlocksListModel;

var Observer = require('../common/Observer'),
	TextBlockItemComponent = require('../TextBlockItem/TextBlockItemFacade');


function TextBlocksListModel (blockItems) {
	var self = this;

	self._items = blockItems;

	self.itemAdded = new Observer(self);
	self.itemRemoved = new Observer(self);
	self.itemSelected = new Observer(self);
	self.itemStateChanged = new Observer(self);
};


TextBlocksListModel.prototype = {
	getItems: function () {
		return [].concat(this._items);
	},

	addItem: function (item) {
		var textBlock,
			that = this,
			textBlockModel;

		textBlock = new TextBlockItemComponent(item);
		textBlockModel = textBlock.model;
		textBlockModel.onRemoved.attach(function (sender) {
			that.removeItemById(sender.id);
		});

		textBlockModel.onSelected.attach(function (sender) {
			that.itemSelected.notify();
		});

		if (textBlockModel.hasOwnProperty('isRed')) {
			textBlockModel.onStateChanged.attach(function (sender) {
				that.itemStateChanged.notify();
			});
		}

		that._items.push(textBlock);
		that.itemAdded.notify({ item : textBlock });
	},

	removeItemById: function (itemId) {
		var that = this,
			removedItem;

		that._items.forEach(function(item, i) {
			if (item.model.id === itemId) {
				that._items.splice(i, 1);
				removedItem = item;
			}
		});

		if (removedItem) {
			delete removedItem.model;
			delete removedItem.controller;
			delete removedItem.view;
		}

		that.itemRemoved.notify();
	}

};

