/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1);

	var TextBloksListModel = __webpack_require__(2),
		TextBloksListView = __webpack_require__(10),
		TextBlocksListController = __webpack_require__(13);

	var listModel = new TextBloksListModel([]),
		appView = new TextBloksListView(listModel, document.getElementById('TextBlocksBoard')),
		listController = new TextBlocksListController(listModel, appView);

	appView.render();



/***/ },
/* 1 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	module.exports = TextBlocksListModel;

	var Observer = __webpack_require__(3),
		TextBlockItemComponent = __webpack_require__(4);


	function TextBlocksListModel (blockItems) {
		var self = this;

		self._items = blockItems;

		self.itemAdded = new Observer(self);
		self.itemRemoved = new Observer(self);
		self.itemSelected = new Observer(self);
		self.itemStateChanged = new Observer(self);
	};


	TextBlocksListModel.prototype = {
		getItems: function () {
			return [].concat(this._items);
		},

		addItem: function (item) {
			var textBlock,
				that = this,
				textBlockModel;

			textBlock = new TextBlockItemComponent(item);
			textBlockModel = textBlock.model;
			textBlockModel.onRemoved.attach(function (sender) {
				that.removeItemById(sender.id);
			});

			textBlockModel.onSelected.attach(function (sender) {
				that.itemSelected.notify();
			});

			if (textBlockModel.hasOwnProperty('isRed')) {
				textBlockModel.onStateChanged.attach(function (sender) {
					that.itemStateChanged.notify();
				});
			}

			that._items.push(textBlock);
			that.itemAdded.notify({ item : textBlock });
		},

		removeItemById: function (itemId) {
			var that = this,
				removedItem;

			that._items.forEach(function(item, i) {
				if (item.model.id === itemId) {
					that._items.splice(i, 1);
					removedItem = item;
				}
			});

			if (removedItem) {
				delete removedItem.model;
				delete removedItem.controller;
				delete removedItem.view;
			}

			that.itemRemoved.notify();
		}

	};



/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';

	module.exports = Observer;


	function Observer (sender) {
		var self = this;

		self._sender = sender;
	    self._listeners = [];
	};


	Observer.prototype = {
	    attach: function (listener) {
	        this._listeners.push(listener);
	    },

	    detachListners: function () {
	        this._listeners.splice(0, this._listeners.length);
	    },
	    
	    notify: function (args) {
	        var index;

	        for (index = 0; index < this._listeners.length; index += 1) {
	            this._listeners[index](this._sender, args);
	        }
	    }
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';


	module.exports = TextBlockItemFacade;

	var TextBlockItemView = __webpack_require__(5),
		TextBlockItemModel = __webpack_require__(8),
		TextBlockItemController = __webpack_require__(9);


	function TextBlockItemFacade(item) {
		var textBlockModel;

		if (item.isDifficult) {
			textBlockModel =  new TextBlockItemModel.Difficult(item);
		} else {
			textBlockModel = new TextBlockItemModel.Usual();
		} 

		var	textBlockView = new TextBlockItemView(textBlockModel),
			textBlockController = new TextBlockItemController(textBlockModel, textBlockView);

		return {
			model: textBlockModel,
			view: textBlockView,
			controller: textBlockController
		};
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';


	module.exports = TextBlockItemView;

	__webpack_require__(6);

	var blockTemplate = __webpack_require__(7),
		Observer = __webpack_require__(3),
		TextBlockItemModel = __webpack_require__(8);


	function TextBlockItemView(model) {
		var self = this;

		self._model = model;

		
		self.delButtonClicked = new Observer(self);
		self.blockSelected = new Observer(self);
		self.stateChanged = new Observer(self);

		self.delButtonHook = function () {
			self.delButtonClicked.notify();
		}

		self.selectBlockHook = function () {
			self.blockSelected.notify();
		}

		self.changeStateOfDifficultBlockHook = function () {
			self.stateChanged.notify();
		}
	};


	TextBlockItemView.prototype = {
		renderTo: function (wrapper) {
			var that = this,
				temp = document.createElement('div'),
				thatBlockIsDifficult = that._model instanceof TextBlockItemModel.Difficult;

			temp.innerHTML = blockTemplate;
			that._elementContainer = temp.firstChild;
			that._element = temp.querySelector('.text-block');
			that._element.querySelector('.text-block__content').innerHTML = that._model.content;



			if (thatBlockIsDifficult) {
				that._element.classList.add('text-block--difficult')
			}

			if (that._model.isSelected) {
				that._element.classList.add('text-block--selected');
			}
			
			if (!that._model.isRed && thatBlockIsDifficult) {
				that._element.classList.add('text-block--green');
			} else if (that._model.isRed) {
				that._element.classList.add('text-block--red');
			}

			wrapper.appendChild(that._elementContainer);
		},

		getElement: function () {
			return this._element;
		},

	};




/***/ },
/* 6 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = "<div class=\"blocks-list__item block_container\">\r\n\t<div class=\"block_container__core text-block\">\r\n\t\t<div class=\"text-block__bar text-block-bar\">\r\n\t\t\t<div class=\"text-block-bar__type-label type-label\">\r\n\t\t\t\t<svg class=\"type-label__usual\" viewBox=\"-6 -6 36 36\">\r\n\t\t\t\t\t<use xlink:href=\"#usualBlockIcon\" width=\"24\" height=\"24\"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t\t<svg class=\"type-label__difficult\" viewBox=\"-6 -6 36 36\">\r\n\t\t\t\t\t<use xlink:href=\"#difficultBlockIcon\" width=\"24\" height=\"24\"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</div>\r\n\t\t\t<svg class=\"text-block-bar__delete-btn\" viewBox=\"-6 -6 36 36\">\r\n\t\t\t\t<use xlink:href=\"#clearBlockIcon\" width=\"24\" height=\"24\"></use>\r\n\t\t\t</svg>\r\n\t\t</div>\r\n\t\t<div class=\"text-block__content\"></div>\r\n\t</div>\r\n</div>";

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';


	module.exports = {
		Usual: TextBlockItemModel,
		Difficult: DifficultTextBlockItemModel
	};

	var Observer = __webpack_require__(3);

	var currentId = 0;


	function TextBlockItemModel() {
		var self = this;

		self.id = self.generateId();
		self.onRemoved = new Observer(self);
		self.onSelected = new Observer(self);
		self.content = "";
	};


	TextBlockItemModel.prototype = {
		generateId: function () {
			var newId = ++currentId;
			return newId;
		},

		getId: function () {
			return this.id;
		},

		getItem: function () {
			return this;
		},

		select: function () {
			this.isSelected = !this.isSelected;
			this.onSelected.notify();
		},

		remove: function () {
			this.onRemoved.notify();
		}
	};


	function DifficultTextBlockItemModel() {
		var self = this;

		self.id = self.generateId();
		self.onRemoved = new Observer(self);
		self.onSelected = new Observer(self);
		self.onStateChanged = new Observer(self);
		self.isRed = true;

	}

	DifficultTextBlockItemModel.prototype = Object.create(TextBlockItemModel.prototype);

	DifficultTextBlockItemModel.prototype.changeState = function () {
		var that = this;

		that.isRed = !that.isRed;
		that.onStateChanged.notify();
	}

/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';


	module.exports = TextBlockItemController;


	function TextBlockItemController(model, view) {
		var self = this;

		self._model = model;
		self._view = view;

		self._view.delButtonClicked.attach(function() {
			self.removeCurrentItem();
		});

		self._view.blockSelected.attach(function () {
			self.selectCurrentItem();
		});

		self._view.stateChanged.attach(function () {
			self.changeStateOfCurrentItem();
		});
	};


	TextBlockItemController.prototype = {
		selectCurrentItem: function () {
			this._model.select();
		},

		removeCurrentItem: function () {
			var answer = window.confirm('Вы действительно хотите удалить этот блок?');
			answer ? this._model.remove() : null;
		},

		changeStateOfCurrentItem: function () {
			this._model.changeState();
		}

	};

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	module.exports = TextBlocksListView;

	__webpack_require__(11);
	__webpack_require__(12);

	var Observer = __webpack_require__(3);


	function TextBlocksListView (model, element) {
		var self = this;

		self._model = model;
	    self._elements = element;

	    self.listModified = new Observer(self);
	    self.addButtonClicked = new Observer(self);
	    self.delButtonDelegateClick = new Observer(self);

	    
	    self._model.itemAdded.attach(function () {
	        self.rebuildList();
	    });
	    self._model.itemRemoved.attach(function () {
	        self.rebuildList();
	    });
	    self._model.itemSelected.attach(function () {
	        self.rebuildList();
	    });
	    self._model.itemStateChanged.attach(function () {
	        self.rebuildList();
	    });

	    self._elements.querySelector('#addButton').addEventListener('click', function () {
	        this.parentNode.querySelector('.embedded-actions').classList.toggle('embedded-actions--open');
	    });


	    self._elements.querySelector('.embedded-actions').addEventListener('click', function (event) {
	        var targetNode = event.target,
	            buttonNode = targetNode.closest('.embedded-action');

	        if (buttonNode.classList.contains('embedded-actions__create-usual')) {
	            self.addButtonClicked.notify({isDifficult: false});
	        } else if(buttonNode.classList.contains('embedded-actions__create-difficult')) {
	            self.addButtonClicked.notify({isDifficult: true});
	        } else {
	            return;
	        }
	    });
	    
	    var timer = 0;
	    var delay = 200;
	    var prevent = false;
	    self._elements.querySelector('#BlocksContainer').addEventListener('click', function(event) {
	        timer = setTimeout(function() {
	            if (!prevent) {
	                listAreaClickAction(event);
	            }
	            prevent = false;
	        }, delay);
	    });
	    self._elements.querySelector('#BlocksContainer').addEventListener('dblclick', function(event) {
	        clearTimeout(timer);
	        prevent = true;
	        listAreaDoubleClickAction(event);
	    });


	    function listAreaClickAction(event) {
	        var targetNode = event.target,
	            delButtonNode = targetNode.closest('.text-block-bar__delete-btn'),
	            blockNodeContainer = targetNode.closest('.blocks-list__item'),
	            blockNode = targetNode.closest('.text-block'),
	            siblingIndex = 0;

	        var fabState = self._elements.querySelector('.embedded-actions').classList;

	        if (fabState.contains('embedded-actions--open')) {
	            fabState.toggle('embedded-actions--open');
	        }
	        

	        if (delButtonNode) {
	            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
	            self.delButtonDelegateClick.notify({removedBlockIndex: siblingIndex});
	        } else if(blockNode && !targetNode.classList.contains('text-block-bar__delete-btn')) {
	            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
	            self.listModified.notify({selectedBlockIndex: siblingIndex});
	        } else {
	            return;
	        }
	       
	    };

	    function listAreaDoubleClickAction(event) {
	        var targetNode = event.target,
	            delButtonNode = targetNode.closest('.text-block-bar__delete-btn'),
	            blockNodeContainer = targetNode.closest('.blocks-list__item'),
	            blockNode = targetNode.closest('.text-block'),
	            siblingIndex = 0;

	        var fabState = self._elements.querySelector('.embedded-actions').classList;

	        if (fabState.contains('embedded-actions--open')) {
	            fabState.toggle('embedded-actions--open');
	        }
	        

	        if (delButtonNode) {
	            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
	            self.delButtonDelegateClick.notify({removedBlockIndex: siblingIndex});
	        } else if(blockNode && !targetNode.classList.contains('text-block-bar__delete-btn') && blockNode.classList.contains('text-block--difficult')) {
	            while( (blockNodeContainer = blockNodeContainer.previousSibling) != null ) siblingIndex++;
	            self.listModified.notify({changedStateBlockIndex: siblingIndex});
	        } else {
	            return;
	        }
	    };
	};


	TextBlocksListView.prototype = {
		render: function () {
	        this.rebuildList();
	    },

	    rebuildList: function () {
	        var list,
	        	that = this,
	        	items,
	        	key,
	        	textBlockComponent,
	            blockState,
	        	statePanel,
	            amountSelectedBlocks = 0,
	            amountRedBlocks = 0,
	            amountGreenBlocks = 0,
	            amountSelectedString = ", выбрано: ",
	            amountRedBlocksString = ", красных: ",
	            amountGreenBlocksString = ", зелёных: ";

	        list = document.getElementById('BlocksContainer');
	        list.innerHTML = '';
	        statePanel = that._elements.querySelector('.top-bar__state-panel');


	        that.delButtonDelegateClick.detachListners();
	        that.listModified.detachListners();

	        items = that._model.getItems();

	        for (key in items) {
				items[key].view.renderTo(list);
	            if (items[key].model.isSelected) {
	                amountSelectedBlocks++;
	            }
	            if (items[key].model.isRed) {
	                amountRedBlocks++;
	            }
	            if (items[key].model.isRed === false) {
	                amountGreenBlocks++;
	            }
	        }

	        that.delButtonDelegateClick.attach(function (sender, args) {
	        	items[args.removedBlockIndex].view.delButtonHook();
	        });

	        that.listModified.attach(function (sender, args) {
	        	if (args.hasOwnProperty('selectedBlockIndex')) {
	        		items[args.selectedBlockIndex].view.selectBlockHook();
	        	}

	            if (args.hasOwnProperty('changedStateBlockIndex')) {
	                items[args.changedStateBlockIndex].view.changeStateOfDifficultBlockHook();
	            }
	        });

	        amountSelectedString = amountSelectedBlocks ? amountSelectedString + amountSelectedBlocks : '';
	        amountRedBlocksString = amountRedBlocks ? amountRedBlocksString + amountRedBlocks : '';
	        amountGreenBlocksString = amountGreenBlocks ? amountGreenBlocksString + amountGreenBlocks : '';
	     	statePanel.innerHTML = "  Общее количество блоков: " + items.length + amountSelectedString + amountRedBlocksString + amountGreenBlocksString;

	    }

	};


/***/ },
/* 11 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 12 */
/***/ function(module, exports) {

	'use strict';


	// polyfills
	(function(e){

		e.matches || (e.matches=e.matchesSelector||function(selector){
		  var matches = document.querySelectorAll(selector), th = this;
		  return Array.prototype.some.call(matches, function(e){
		     return e === th;
		  });
		});

		e.closest = e.closest || function closest(css){
		    return this.parentNode ? 
		        this.matches(css) ? this : closest.call(this.parentNode, css) 
		    : null;
		}

	})(Element.prototype);




/***/ },
/* 13 */
/***/ function(module, exports) {

	'use strict';

	module.exports = TextBlocksListController;



	function TextBlocksListController(model, view) {
	    var self = this;

		self._model = model;
	    self._view = view;


	    self._view.addButtonClicked.attach(function(sender, args) {
	        self.addItem(args);
	    });

	};


	TextBlocksListController.prototype = {
	    addItem: function (newItemProps) {
	        this._model.addItem(newItemProps);
	    },
	   
	};

/***/ }
/******/ ]);